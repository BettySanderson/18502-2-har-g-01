This folder should contain all Altium files generated during the project. 
It does not need to include any outputs as these will be stored in the PCB Deliverables Folder.

The contents of the Altium project folder should contain 3 additional folders and a project file. The folders are
- PCB
- Schematics
- Libraries

and the project file name should be called PRJ-XXXXX-A-01.PrjPcb (E.g. PRJ-12376-B-04.PrjPcb)

Schematics file names should begin with the prefix SCH at the start of every file name, followed by Project number, Board Letter and revision (SCH-12376-B-04.sch).
Title pages will only contain the Board letter and the rest of the schematic files will contain a unique additional letter for every consecutive page.

Title Page - e.g. SCH-12376-B-04.sch
Schematic Page - e.g. SCH-12376-BA-04.sch

PCB file names should begin with the prefix PCB at the start of every file name, followed by Project number, Board Letter and revision (PCB-12376-B-04.pcb).


Out Job File.s

This will help designers to quickly create all the outputs necessary for a project. Depending on the board there are different job files for different layers of PCB's. Keep the template that matches your project and you can delete the rest of the outjob files.